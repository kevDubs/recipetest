import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

import './App.css';

var isRecipe = false;

class App extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {};
    this.handleClickMeals = this.handleClickMeals.bind(this);
    this.handleClickRecipe = this.handleClickRecipe.bind(this);
   
    this.handleJSON = this.handleJSON.bind(this);
  }
  handleClickMeals(e, param) {
    var meals = 'filter.php?c='+param
    this.handleJSON(meals);
  }
  handleClickRecipe(e, param) {
    isRecipe = true;
    var recipe = 'lookup.php?i='+param
    this.handleJSON(recipe);
  }
  handleJSON(param){
    var that = this;
    var url = 'https://www.themealdb.com/api/json/v1/1/'+param;
    
  
    fetch(url)
    .then(res => res.json())
        .then(
          (result) => {
          
            if(result.categories){
              this.setState({apiCats: result.categories});
            }
            if(result.meals && !isRecipe){
              this.setState({apiMeals: result.meals});
            }
            if(result.meals && isRecipe){
              this.setState({apiRecipe: result.meals});
            }
            
           
            
            this.setState({
              isLoaded: true,
              items: result.items
            });
          },
  
          (error) => {
            this.setState({
              isLoaded: true,
              error
            });
          }
        ) 
  }
  componentDidMount() {
    this.handleJSON('categories.php');
  }
  
  renderContent(){
   
    if (!this.state.apiCats) {
      return <span>Loading...</span>;
    }else{
      return <div className="cat-holder">
      {this.state.apiCats.map((item, index) => (
        <CatItem key={index} thumb={item.strCategoryThumb} value={item.strCategory} desc={item.strCategoryDescription} handleClick={this.handleClickMeals} />
      ))}
      </div>
    }

    
  }
  renderContentMeals(){
    if (!this.state.apiMeals) {
      return <span>Loading...</span>;
    }else{
      return <div className="cat-holder">
      {this.state.apiMeals.map((item, index) => (
        <MealsItem key={index} thumb={item.strMealThumb} value={item.strMeal} idMeal={item.idMeal} handleClick={this.handleClickRecipe} />
      ))}
    </div>
    }
  }

  renderContentRecipe(){
   var strMeasureArry = [];
   var strIngredientArry = [];

    if (!this.state.apiRecipe) {
      return <span>Recipe Loading...</span>;
    }else{
    

      for(var prop in this.state.apiRecipe[0]) {
 
        if(prop.includes("strMeasure") && this.state.apiRecipe[0][prop].length > 0){
          strMeasureArry.push(this.state.apiRecipe[0][prop]);
        }
        if(prop.includes("strIngredient") && this.state.apiRecipe[0][prop].length > 0){
          strIngredientArry.push(this.state.apiRecipe[0][prop]);
        }
      }
      
      return <div>
      {this.state.apiRecipe.map((item, index) => (
        <RecipeItem key={index} thumb={item.strMealThumb} value={item.strMeal} measurment={strMeasureArry} ingredient={strIngredientArry} strInstructions={item.strInstructions} />
      ))}
    </div>
    }
  }
 
  render() {
    
    return (
      <Router>
        <div>
        <Header/>
          <Switch>
            <Route exact path="/" exact={true}>
              {this.renderContent()}
            </Route>
            <Route exact path="/Meals" exact={false}>
              {this.renderContentMeals()}
            </Route>
            <Route exact path="/Recipes" exact={false}>
              {this.renderContentRecipe()}
            </Route>
          </Switch>
         
        </div>
        
      </Router>
    );
    
  }
  
}

class Header extends React.Component {
  render() {
    return <div className="App-header">
      <h1>REGAL RECIPES</h1>
     
      </div>
  }
}



class CatItem extends React.Component {
  render() {
    return <div className="cat-item">
      <img src={this.props.thumb} alt={this.props.value}></img>
        <div>
          <h1>{this.props.value}</h1>
          <p>{this.props.desc}</p>
          
          <Link to={"/Meals/"+this.props.value} onClick={(e) => { this.props.handleClick(e, this.props.value)}}>More {this.props.value} Recipes </Link>
        </div>
     
      </div>
  }
}

class MealsItem extends React.Component {
  
  render() {
    return <div className="cat-item">
      <img src={this.props.thumb} alt={this.props.value}></img>
        <div>
          <h1>{this.props.value}</h1>
          <p>{this.props.idMeal}</p>
          <Link to={"/Recipes/"+this.props.value} onClick={(e) => { this.props.handleClick(e, this.props.idMeal)}}>Get {this.props.value} Recipe </Link>
        </div>
     
      </div>
  }
}


class RecipeItem extends React.Component {

  renderContent(){
    console.log(this.props.measurment);
  }
  
  render() {
    const listItems = this.props.measurment.map((item, index) =>
      <li key={index}>{item} - {this.props.ingredient[index]}</li> 
    );
    return <div className="recipe-item">
     
      <img src={this.props.thumb} alt={this.props.value}></img>
      <div class="recipe-text">
        
        <div>
          <h1>{this.props.value}</h1>
          <ul>
            {listItems}
          </ul>
          <p>{this.props.strInstructions}</p>
        </div>

      </div>
      
      </div>
  }
}



export default App;


